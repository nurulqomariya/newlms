{{-- <!DOCTYPE html>
    <html>

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>Laravel</title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
            <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
            <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
            <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
            <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" />
            <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" />
            <link href="https://unpkg.com/@coreui/coreui@2.1.16/dist/css/coreui.min.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
            <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
        </head>

        <body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done sidebar-lg-show">
            <header class="app-header navbar">
                <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <span class="navbar-brand-full">Admin</span>
                    <span class="navbar-brand-minimized">Panel</span>
                </a>
                <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <ul class="nav navbar-nav ml-auto">
                    @if(count(config('panel.available_languages', [])) > 1)
                        <li class="nav-item dropdown d-md-down-none">
                            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                {{ strtoupper(app()->getLocale()) }}
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                @foreach(config('panel.available_languages') as $langLocale => $langName)
                                    <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a>
                                @endforeach
                            </div>
                        </li>
                    @endif


                </ul>
            </header>

            <div class="app-body">
                @include('partials.menu')
                <main class="main">


                    <div style="padding-top: 20px" class="container-fluid">
                        @if(session('message'))
                            <div class="row mb-2">
                                <div class="col-lg-12">
                                    <div class="alert alert-success" role="alert">{{ session('message') }}</div>
                                </div>
                            </div>
                        @endif
                        @if($errors->count() > 0)
                            <div class="alert alert-danger">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @yield('content')

                    </div>


                </main>
                <form id="logoutform" action="" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
            <script src="https://unpkg.com/@coreui/coreui@2.1.16/dist/js/coreui.min.js"></script>
            <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
            <script src="//cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
            <script src="//cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.colVis.min.js"></script>
            <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
            <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
            <script src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
            <script src="https://cdn.ckeditor.com/ckeditor5/11.0.1/classic/ckeditor.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
            <script src="{{ asset('js/main.js') }}"></script>
            @stack('script-alt')
        </body>

    </html> --}}

<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Admin </title>
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset ('assetfocus/images/favicon.png') }}">
        <link rel="stylesheet" href="{{ asset ('assetfocus/vendor/owl-carousel/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset ('assetfocus/vendor/owl-carousel/css/owl.theme.default.min.css') }}">
        <link href="{{ asset ('assetfocus/vendor/jqvmap/css/jqvmap.min.css') }}" rel="stylesheet">
        <link href="{{ asset ('assetfocus/css/style.css') }}" rel="stylesheet">
    
    
    
    </head>
    
    <body>
    
        <!--*******************
            Preloader start
        ********************-->
        <div id="preloader">
            <div class="sk-three-bounce">
                <div class="sk-child sk-bounce1"></div>
                <div class="sk-child sk-bounce2"></div>
                <div class="sk-child sk-bounce3"></div>
            </div>
        </div>
        <!--*******************
            Preloader end
        ********************-->
    
    
        <!--**********************************
            Main wrapper start
        ***********************************-->
        <div id="main-wrapper">
    
            <!--**********************************
                Nav header start
            ***********************************-->
            <div class="nav-header">
                {{-- <a href="index.html" class="brand-logo">
                    <img class="logo-abbr" src="./images/logo.png" alt="">
                    <img class="logo-compact" src="./images/logo-text.png" alt="">
                    <img class="brand-title" src="./images/logo-text.png" alt="">
                </a> --}}
    
                {{-- <div class="nav-control">
                    <div class="hamburger">
                        <span class="line"></span><span class="line"></span><span class="line"></span>
                    </div>
                </div> --}}
            </div>
            <!--**********************************
                Nav header end
            ***********************************-->
    
            <!--**********************************
                Header start
            ***********************************-->
            <div class="header">
                <div class="header-content">
                    <nav class="navbar navbar-expand">
                        <div class="collapse navbar-collapse justify-content-between">
                            <div class="header-left">
                                {{-- <div class="search_bar dropdown">
                                    <span class="search_icon p-3 c-pointer" data-toggle="dropdown">
                                        <i class="mdi mdi-magnify"></i>
                                    </span>
                                    <div class="dropdown-menu p-0 m-0">
                                        <form>
                                            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                        </form>
                                    </div>
                                </div> --}}
                            </div>
    
                            <ul class="navbar-nav header-right">
                                {{-- <li class="nav-item dropdown notification_dropdown">
                                    <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                        <i class="mdi mdi-bell"></i>
                                        <div class="pulse-css"></div>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="list-unstyled">
                                            <li class="media dropdown-item">
                                                <span class="success"><i class="ti-user"></i></span>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <p><strong>Martin</strong> has added a <strong>customer</strong> Successfully
                                                        </p>
                                                    </a>
                                                </div>
                                                <span class="notify-time">3:20 am</span>
                                            </li>
                                            <li class="media dropdown-item">
                                                <span class="primary"><i class="ti-shopping-cart"></i></span>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <p><strong>Jennifer</strong> purchased Light Dashboard 2.0.</p>
                                                    </a>
                                                </div>
                                                <span class="notify-time">3:20 am</span>
                                            </li>
                                            <li class="media dropdown-item">
                                                <span class="danger"><i class="ti-bookmark"></i></span>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <p><strong>Robin</strong> marked a <strong>ticket</strong> as unsolved.
                                                        </p>
                                                    </a>
                                                </div>
                                                <span class="notify-time">3:20 am</span>
                                            </li>
                                            <li class="media dropdown-item">
                                                <span class="primary"><i class="ti-heart"></i></span>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <p><strong>David</strong> purchased Light Dashboard 1.0.</p>
                                                    </a>
                                                </div>
                                                <span class="notify-time">3:20 am</span>
                                            </li>
                                            <li class="media dropdown-item">
                                                <span class="success"><i class="ti-image"></i></span>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <p><strong> James.</strong> has added a<strong>customer</strong> Successfully
                                                        </p>
                                                    </a>
                                                </div>
                                                <span class="notify-time">3:20 am</span>
                                            </li>
                                        </ul>
                                        <a class="all-notification" href="#">See all notifications <i
                                                class="ti-arrow-right"></i></a>
                                    </div>
                                </li> --}}
                                <li class="nav-item dropdown header-profile">
                                    <a class="nav-link" href="#" role="button" data-toggle="dropdown">
                                        <i class="mdi mdi-account"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        {{-- <a href="./app-profile.html" class="dropdown-item">
                                            <i class="icon-user"></i>
                                            <span class="ml-2">Profile </span>
                                        </a>
                                        <a href="./email-inbox.html" class="dropdown-item">
                                            <i class="icon-envelope-open"></i>
                                            <span class="ml-2">Inbox </span>
                                        </a> --}}
                                        <a href="#" onclick="getElementById('logout-form').submit()" class="dropdown-item">
                                            <i class="icon-key"></i>
                                            <span class="ml-2">Logout </span>
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="post">
                                            @csrf 
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!--**********************************
                Header end ti-comment-alt
            ***********************************-->
    
            <!--**********************************
                Sidebar start
            ***********************************-->
            @include('partials.menu')
            <!--**********************************
                Sidebar end
            ***********************************-->
    
            <!--**********************************
                Content body start
            ***********************************-->
            {{-- <div class="content-body">
                <!-- row -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-sm-6">
                            <div class="card">
                                <div class="stat-widget-two card-body">
                                    <div class="stat-content">
                                        <div class="stat-text">Today Expenses </div>
                                        <div class="stat-digit"> <i class="fa fa-usd"></i>8500</div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success w-85" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="card">
                                <div class="stat-widget-two card-body">
                                    <div class="stat-content">
                                        <div class="stat-text">Income Detail</div>
                                        <div class="stat-digit"> <i class="fa fa-usd"></i>7800</div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary w-75" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="card">
                                <div class="stat-widget-two card-body">
                                    <div class="stat-content">
                                        <div class="stat-text">Task Completed</div>
                                        <div class="stat-digit"> <i class="fa fa-usd"></i> 500</div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="card">
                                <div class="stat-widget-two card-body">
                                    <div class="stat-content">
                                        <div class="stat-text">Task Completed</div>
                                        <div class="stat-digit"> <i class="fa fa-usd"></i>650</div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger w-65" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 col-md-8">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Sales Overview</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-8">
                                            <div id="morris-bar-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="m-t-10">
                                        <h4 class="card-title">Customer Feedback</h4>
                                        <h2 class="mt-3">385749</h2>
                                    </div>
                                    <div class="widget-card-circle mt-5 mb-5" id="info-circle-card">
                                        <i class="ti-control-shuffle pa"></i>
                                    </div>
                                    <ul class="widget-line-list m-b-15">
                                        <li class="border-right">92% <br><span class="text-success"><i
                                                    class="ti-hand-point-up"></i> Positive</span></li>
                                        <li>8% <br><span class="text-danger"><i
                                                    class="ti-hand-point-down"></i>Negative</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Project</h4>
                                </div>
                                <div class="card-body">
                                    <div class="current-progress">
                                        <div class="progress-content py-2">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="progress-text">Website</div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="current-progressbar">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-primary w-40" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                                40%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-content py-2">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="progress-text">Android</div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="current-progressbar">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-primary w-60" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                                                                60%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-content py-2">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="progress-text">Ios</div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="current-progressbar">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-primary w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                                70%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-content py-2">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="progress-text">Mobile</div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <div class="current-progressbar">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-primary w-90" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                                                                90%
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="testimonial-widget-one p-17">
                                        <div class="testimonial-widget-one owl-carousel owl-theme">
                                            <div class="item">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-text">
                                                        <i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                                                        consectetur adipisicing elit.
                                                        <i class="fa fa-quote-right"></i>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <div class="testimonial-author">TYRION LANNISTER</div>
                                                            <div class="testimonial-author-position">Founder-Ceo. Dell Corp
                                                            </div>
                                                        </div>
                                                        <img class="testimonial-author-img ml-3" src="./images/avatar/1.png" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-text">
                                                        <i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                                                        consectetur adipisicing elit.
                                                        <i class="fa fa-quote-right"></i>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <div class="testimonial-author">TYRION LANNISTER</div>
                                                            <div class="testimonial-author-position">Founder-Ceo. Dell Corp
                                                            </div>
                                                        </div>
                                                        <img class="testimonial-author-img ml-3" src="./images/avatar/1.png" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="testimonial-content">
                                                    <div class="testimonial-text">
                                                        <i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.
                                                        consectetur adipisicing elit.
                                                        <i class="fa fa-quote-right"></i>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <div class="testimonial-author">TYRION LANNISTER</div>
                                                            <div class="testimonial-author-position">Founder-Ceo. Dell Corp
                                                            </div>
                                                        </div>
                                                        <img class="testimonial-author-img ml-3" src="./images/avatar/1.png" alt="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Web Server</h4>
                                </div>
                                <div class="card-body">
                                    <div class="cpu-load-chart">
                                        <div id="cpu-load" class="cpu-load"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Country</h4>
                                </div>
                                <div class="card-body">
                                    <div id="vmap13" class="vmap"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">New Orders</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table mb-0">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Product</th>
                                                    <th>quantity</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="round-img">
                                                            <a href=""><img width="35" src="./images/avatar/1.png" alt=""></a>
                                                        </div>
                                                    </td>
                                                    <td>Lew Shawon</td>
                                                    <td><span>Dell-985</span></td>
                                                    <td><span>456 pcs</span></td>
                                                    <td><span class="badge badge-success">Done</span></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round-img">
                                                            <a href=""><img width="35" src="./images/avatar/1.png" alt=""></a>
                                                        </div>
                                                    </td>
                                                    <td>Lew Shawon</td>
                                                    <td><span>Asus-565</span></td>
                                                    <td><span>456 pcs</span></td>
                                                    <td><span class="badge badge-warning">Pending</span></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round-img">
                                                            <a href=""><img width="35" src="./images/avatar/1.png" alt=""></a>
                                                        </div>
                                                    </td>
                                                    <td>lew Shawon</td>
                                                    <td><span>Dell-985</span></td>
                                                    <td><span>456 pcs</span></td>
                                                    <td><span class="badge badge-success">Done</span></td>
                                                </tr>
    
                                                <tr>
                                                    <td>
                                                        <div class="round-img">
                                                            <a href=""><img width="35" src="./images/avatar/1.png" alt=""></a>
                                                        </div>
                                                    </td>
                                                    <td>Lew Shawon</td>
                                                    <td><span>Asus-565</span></td>
                                                    <td><span>456 pcs</span></td>
                                                    <td><span class="badge badge-warning">Pending</span></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="round-img">
                                                            <a href=""><img width="35" src="./images/avatar/1.png" alt=""></a>
                                                        </div>
                                                    </td>
                                                    <td>lew Shawon</td>
                                                    <td><span>Dell-985</span></td>
                                                    <td><span>456 pcs</span></td>
                                                    <td><span class="badge badge-success">Done</span></td>
                                                </tr>
    
                                                <tr>
                                                    <td>
                                                        <div class="round-img">
                                                            <a href=""><img width="35" src="./images/avatar/1.png" alt=""></a>
                                                        </div>
                                                    </td>
                                                    <td>Lew Shawon</td>
                                                    <td><span>Asus-565</span></td>
                                                    <td><span>456 pcs</span></td>
                                                    <td><span class="badge badge-warning">Pending</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xl-4 col-xxl-6 col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Timeline</h4>
                                </div>
                                <div class="card-body">
                                    <div class="widget-timeline">
                                        <ul class="timeline">
                                            <li>
                                                <div class="timeline-badge primary"></div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>10 minutes ago</span>
                                                    <h6 class="m-t-5">Youtube, a video-sharing website, goes live.</h6>
                                                </a>
                                            </li>
    
                                            <li>
                                                <div class="timeline-badge warning">
                                                </div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>20 minutes ago</span>
                                                    <h6 class="m-t-5">Mashable, a news website and blog, goes live.</h6>
                                                </a>
                                            </li>
    
                                            <li>
                                                <div class="timeline-badge danger">
                                                </div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>30 minutes ago</span>
                                                    <h6 class="m-t-5">Google acquires Youtube.</h6>
                                                </a>
                                            </li>
    
                                            <li>
                                                <div class="timeline-badge success">
                                                </div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>15 minutes ago</span>
                                                    <h6 class="m-t-5">StumbleUpon is acquired by eBay. </h6>
                                                </a>
                                            </li>
    
                                            <li>
                                                <div class="timeline-badge warning">
                                                </div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>20 minutes ago</span>
                                                    <h6 class="m-t-5">Mashable, a news website and blog, goes live.</h6>
                                                </a>
                                            </li>
    
                                            <li>
                                                <div class="timeline-badge dark">
                                                </div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>20 minutes ago</span>
                                                    <h6 class="m-t-5">Mashable, a news website and blog, goes live.</h6>
                                                </a>
                                            </li>
    
                                            <li>
                                                <div class="timeline-badge info">
                                                </div>
                                                <a class="timeline-panel text-muted" href="#">
                                                    <span>30 minutes ago</span>
                                                    <h6 class="m-t-5">Google acquires Youtube.</h6>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-xxl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Todo</h4>
                                </div>
                                <div class="card-body px-0">
                                    <div class="todo-list">
                                        <div class="tdl-holder">
                                            <div class="tdl-content widget-todo mr-4">
                                                <ul id="todo_list">
                                                    <li><label><input type="checkbox"><i></i><span>Get up</span><a href='#'
                                                                class="ti-trash"></a></label></li>
                                                    <li><label><input type="checkbox" checked><i></i><span>Stand up</span><a
                                                                href='#' class="ti-trash"></a></label></li>
                                                    <li><label><input type="checkbox"><i></i><span>Don't give up the
                                                                fight.</span><a href='#' class="ti-trash"></a></label></li>
                                                    <li><label><input type="checkbox" checked><i></i><span>Do something
                                                                else</span><a href='#' class="ti-trash"></a></label></li>
                                                    <li><label><input type="checkbox" checked><i></i><span>Stand up</span><a
                                                                href='#' class="ti-trash"></a></label></li>
                                                    <li><label><input type="checkbox"><i></i><span>Don't give up the
                                                                fight.</span><a href='#' class="ti-trash"></a></label></li>
                                                </ul>
                                            </div>
                                            <div class="px-4">
                                                <input type="text" class="tdl-new form-control" placeholder="Write new item and hit 'Enter'...">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-xxl-6 col-xl-4 col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Product Sold</h4>
                                    <div class="card-action">
                                        <div class="dropdown custom-dropdown">
                                            <div data-toggle="dropdown">
                                                <i class="ti-more-alt"></i>
                                            </div>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Option 1</a>
                                                <a class="dropdown-item" href="#">Option 2</a>
                                                <a class="dropdown-item" href="#">Option 3</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="chart py-4">
                                        <canvas id="sold-product"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-xxl-6 col-lg-6 col-md-12">
                            <div class="row">
                                <div class="col-xl-3 col-lg-6 col-sm-6 col-xxl-6 col-md-6">
                                    <div class="card">
                                        <div class="social-graph-wrapper widget-facebook">
                                            <span class="s-icon"><i class="fa fa-facebook"></i></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 border-right">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">89</span> k</h4>
                                                    <p class="m-0">Friends</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">119</span> k</h4>
                                                    <p class="m-0">Followers</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-sm-6 col-xxl-6 col-md-6">
                                    <div class="card">
                                        <div class="social-graph-wrapper widget-linkedin">
                                            <span class="s-icon"><i class="fa fa-linkedin"></i></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 border-right">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">89</span> k</h4>
                                                    <p class="m-0">Friends</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">119</span> k</h4>
                                                    <p class="m-0">Followers</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-sm-6 col-xxl-6 col-md-6">
                                    <div class="card">
                                        <div class="social-graph-wrapper widget-googleplus">
                                            <span class="s-icon"><i class="fa fa-google-plus"></i></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 border-right">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">89</span> k</h4>
                                                    <p class="m-0">Friends</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">119</span> k</h4>
                                                    <p class="m-0">Followers</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-sm-6 col-xxl-6 col-md-6">
                                    <div class="card">
                                        <div class="social-graph-wrapper widget-twitter">
                                            <span class="s-icon"><i class="fa fa-twitter"></i></span>
                                        </div>
                                        <div class="row">
                                            <div class="col-6 border-right">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">89</span> k</h4>
                                                    <p class="m-0">Friends</p>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="pt-3 pb-3 pl-0 pr-0 text-center">
                                                    <h4 class="m-1"><span class="counter">119</span> k</h4>
                                                    <p class="m-0">Followers</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                </div>
            </div> --}}
            @yield('content')
            <!--**********************************
                Content body end
            ***********************************-->
    
    
            <!--**********************************
                Footer start
            ***********************************-->
            <div class="footer">
                <div class="copyright">
                    <p>Copyright © Designed &amp; Developed by <a href="#" target="_blank">Quixkit</a> 2019</p>
                    {{-- <p>Distributed by <a href="https://themewagon.com/" target="_blank">Themewagon</a></p>  --}}
                </div>
            </div>
            <!--**********************************
                Footer end
            ***********************************-->
    
            <!--**********************************
               Support ticket button start
            ***********************************-->
    
            <!--**********************************
               Support ticket button end
            ***********************************-->
    
    
        </div>
        <!--**********************************
            Main wrapper end
        ***********************************-->
    
        <!--**********************************
            Scripts
        ***********************************-->
        <!-- Required vendors -->
        <script src="{{ asset ('assetfocus/vendor/global/global.min.js') }}"></script>
        <script src="{{ asset ('assetfocus/js/quixnav-init.js') }}"></script>
        <script src="{{ asset ('assetfocus/js/custom.min.js') }}"></script>
    
    
        <!-- Vectormap -->
        <script src="{{ asset ('assetfocus/vendor/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset ('assetfocus/vendor/morris/morris.min.js') }}"></script>
    
    
        <script src="{{ asset ('assetfocus/vendor/circle-progress/circle-progress.min.js') }}"></script>
        <script src="{{ asset ('assetfocus/vendor/chart.js') }}/Chart.bundle.min.js') }}"></script>
    
        <script src="{{ asset ('assetfocus/vendor/gaugeJS/dist/gauge.min.js') }}"></script>
    
        <!--  flot-chart js -->
        <script src="{{ asset ('assetfocus/vendor/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset ('assetfocus/vendor/flot/jquery.flot.resize.js') }}"></script>
    
        <!-- Owl Carousel -->
        <script src="{{ asset ('assetfocus/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>
    
        <!-- Counter Up -->
        <script src="{{ asset ('assetfocus/vendor/jqvmap/js/jquery.vmap.min.js') }}"></script>
        <script src="{{ asset ('assetfocus/vendor/jqvmap/js/jquery.vmap.usa.js') }}"></script>
        <script src="{{ asset ('assetfocus/vendor/jquery.counterup/jquery.counterup.min.js') }}"></script>
    
    
        <script src="{{ asset ('assetfocus/js/dashboard/dashboard-1.js') }}"></script>
    
    </body>
    
    </html>