@extends('layouts.front')

@section('content')
      {{-- <section class="products section container" id="course">
        <h2 class="section-title">My Course</h2>

        <div class="featured-container grid">
            @forelse($purchased_courses as $purchased_course)
              <article class="products-card swiper-slide">
                <a style="color: inherit;" href="{{ route('courses.show', [$purchased_course->slug]) }}" class="products-link">
                    <img
                    src="{{ Storage::url($purchased_course->course_image) }}"
                    class="products-img"
                    alt=""
                    />
                    <h3 class="products-title">{{ $purchased_course->title }}</h3>
                    <div class="products-star">
                    @for ($star = 1; $star <= 5; $star++)
                        @if ($purchased_course->rating >= $star)
                        <i class="bx bxs-star"></i>
                        @else
                        <i class='bx bx-star'></i>
                        @endif
                    @endfor
                    </div>
                    <span class="products-price">${{ $purchased_course->price }}</span>
                    @if($purchased_course->students()->count() > 5)
                    <button class="products-button">
                        Popular
                    </button>
                    @endif
                    <span class="products-student">
                    {{ $purchased_course->students()->count() }} students
                    </span>
                </a>
              </article>
            @empty
                <h2 style="text-align: center;grid-column: 1/5">You haven't purchased course yet</h2>
            @endforelse
            </div>
      </section>

      <section class="products section container" id="course">
        <h2 class="section-title">All Course</h2>

        <div class="new-container">
          <div class="swiper new-swipper">
            <div class="swiper-wrapper">
            @foreach($courses as $course)
              <article class="products-card swiper-slide">
              <a style="color: inherit;" href="{{ route('courses.show', [$course->slug]) }}" class="products-link">
                <img
                  src="{{ Storage::url($course->course_image) }}"
                  class="products-img"
                  alt=""
                />
                <h3 class="products-title">{{ $course->title }}</h3>
                <div class="products-star">
                @for ($star = 1; $star <= 5; $star++)
                    @if ($course->rating >= $star)
                    <i class="bx bxs-star"></i>
                    @else
                    <i class='bx bx-star'></i>
                    @endif
                @endfor
                </div>
                <span class="products-price">${{ $course->price }}</span>
                @if($course->students()->count() > 5)
                  <button class="products-button">
                    Popular
                  </button>
                @endif
                <span class="products-student">
                {{ $course->students()->count() }} students
                </span>
              </a>
              </article>
            @endforeach
    
            </div>
            <div
              class="swiper-button-next"
              style="
                bottom: initial;
                top: 50%;
                right: 0;
                left: initial;
                border-radius: 50%;
              "
            >
              <i class="bx bx-right-arrow-alt"></i>
            </div>
            <div
              class="swiper-button-prev"
              style="bottom: initial; top: 50%; border-radius: 50%"
            >
              <i class="bx bx-left-arrow-alt"></i>
            </div>
          </div>
        </div>
      </section> --}}

      <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs">
      <div class="container">
        <h2>Courses</h2>
        {{-- <p>Est dolorum ut non facere possimus quibusdam eligendi voluptatem. Quia id aut similique quia voluptas sit quaerat debitis. Rerum omnis ipsam aperiam consequatur laboriosam nemo harum praesentium. </p> --}}
      </div>
    </div><!-- End Breadcrumbs -->
    <section class="products section container" id="course">
      <div class="section-title">
        <h2>Courses</h2>
        <p>My Courses</p>
      </div>

      <div class="featured-container grid">
          @forelse($purchased_courses as $purchased_course)
            {{-- <article class="products-card swiper-slide">
              <a style="color: inherit;" href="{{ route('courses.show', [$purchased_course->slug]) }}" class="products-link">
                  <img
                  src="{{ Storage::url($purchased_course->course_image) }}"
                  class="products-img"
                  alt=""
                  />
                  <h3 class="products-title">{{ $purchased_course->title }}</h3>
                  <div class="products-star">
                  @for ($star = 1; $star <= 5; $star++)
                      @if ($purchased_course->rating >= $star)
                      <i class="bx bxs-star"></i>
                      @else
                      <i class='bx bx-star'></i>
                      @endif
                  @endfor
                  </div>
                  <span class="products-price">${{ $purchased_course->price }}</span>
                  @if($purchased_course->students()->count() > 5)
                  <button class="products-button">
                      Popular
                  </button>
                  @endif
                  <span class="products-student">
                  {{ $purchased_course->students()->count() }} students
                  </span>
              </a>
            </article> --}}
            <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
              <div class="course-item">
                <img src="{{ Storage::url($purchased_course->course_image) }}" class="img-fluid" alt="...">
                <div class="course-content">
                  {{-- <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4>Web Development</h4>
                    <p class="price">$169</p>
                  </div> --}}

                  <h3><a href="{{ route('courses.show', [$purchased_course->slug]) }}">{{ $purchased_course->title }}</a></h3>
                  {{-- <p>Et architecto provident deleniti facere repellat nobis iste. Id facere quia quae dolores dolorem tempore.</p> --}}
                  @if($purchased_course->students()->count() > 5)
                      <button class="products-button">
                          Popular
                      </button>
                  @endif
                  <div class="trainer d-flex justify-content-between align-items-center">
                    {{-- <div class="trainer-profile d-flex align-items-center">
                      <img src="assets/img/trainers/trainer-1.jpg" class="img-fluid" alt="">
                      <span>Antonio</span>
                    </div> --}}
                    <div class="trainer-rank d-flex align-items-center">
                      <i class="bx bx-user"></i>{{ $purchased_course->students()->count() }}
                      {{-- <i class="bx bx-heart"></i>&nbsp;65 --}}
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- End Course Item-->
          @empty
              <h2 style="text-align: center;grid-column: 1/5">You haven't purchased course yet</h2>
          @endforelse
          </div>
    </section>
      <section id="courses" class="courses">
        <div class="container" data-aos="fade-up">

          <div class="section-title">
            <h2>Courses</h2>
            <p>All Courses</p>
          </div>

  
          <div class="row" data-aos="zoom-in" data-aos-delay="100">
  
            @foreach($courses as $course)              
              <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                <div class="course-item">
                  <img src="{{ Storage::url($course->course_image) }}" class="img-fluid" alt="...">
                  <div class="course-content">
                    {{-- <div class="d-flex justify-content-between align-items-center mb-3">
                      <h4>Web Development</h4>
                      <p class="price">$169</p>
                    </div> --}}
  
                    <h3><a href="{{ route('courses.show', [$course->slug]) }}">{{ $course->title }}</a></h3>
                    {{-- <p>Et architecto provident deleniti facere repellat nobis iste. Id facere quia quae dolores dolorem tempore.</p> --}}
                    @if($course->students()->count() > 5)
                      <button class="products-button">
                        Popular
                      </button>
                    @endif
                    <div class="trainer d-flex justify-content-between align-items-center">
                      {{-- <div class="trainer-profile d-flex align-items-center">
                        <img src="assets/img/trainers/trainer-1.jpg" class="img-fluid" alt="">
                        <span>Antonio</span>
                      </div> --}}
                      <div class="trainer-rank d-flex align-items-center">
                        <i class="bx bx-user"></i>{{ $course->students()->count() }}
                        {{-- <i class="bx bx-heart"></i>&nbsp;65 --}}
                      </div>
                    </div>
                  </div>
                </div>
              </div> <!-- End Course Item-->
            @endforeach
  
          </div>
  
        </div>
      </section><!-- End Courses Section -->
@endsection